var requirejs = require('requirejs');

requirejs.config({
    //Pass the top-level main.js/index.js require
    //function to requirejs so that node modules
    //are loaded relative to the top-level JS file.
    nodeRequire: require
});

requirejs([
	'http',
	'connect',
	'serve-static',
	'fs',
], function (
	http,
	connect,
	serveStatic,
	fs
) {
	var app = connect()
		.use(serveStatic(__dirname + '/frontend'))
		.use(serveStatic(__dirname + '/bower_components'))
		.use(serveStatic(__dirname + '/static'))
		.use(function(req, res) {
			res.end(fs.readFileSync('static/main.html'));
		});

	http.createServer(app).listen(8000);
});