requirejs.config({
    baseUrl: '',
    paths: {
      'app':				'app',
      'angular':		'angular/angular',
    	'bootstrap':	'bootstrap/dist/js/bootstrap',
    	'jquery':			'jquery/dist/jquery',
    },
  	shim: {
			'jquery': {
				exports: '$'
			},
			'bootstrap': {
				deps: [ 'jquery' ],
			},
			'angular': {
				'exports' : 'angular'
			},
			priority: [
				'angular'
			]
		}
});

window.name = "NG_DEFER_BOOTSTRAP!";

requirejs([
	'jquery',
	'bootstrap',
	'angular',
	'app/main',
], function($, bootstrap, angular, app) {
	var $html = angular.element(document.getElementsByTagName('html')[0]);

	angular.element().ready(function() {
		angular.resumeBootstrap([app['name']]);
	});
});