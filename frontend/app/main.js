define([
	'angular'
], function(
	angular
) {
	var app = angular.module('main', []);

	app.controller('BlogController', function() {
		this.title = 'Blogfeld';
		this.tagline = "It's a blog about nothing!";
		this.posts = posts;
	})

	posts = [
		{
			'title': 'Wayfarers pug hashtag, fanny pack cred',
			'body': "Mustache biodiesel PBR brunch tilde freegan, Truffaut Carles cliche umami twee Pinterest. Sustainable sartorial pickled cray. Meh Banksy authentic, scenester bespoke shabby chic polaroid street art letterpress. Food truck semiotics fixie mlkshk freegan, try-hard Neutra polaroid slow-carb. Cliche synth squid sustainable fap kitsch. Next level twee put a bird on it, semiotics narwhal banjo street art. Readymade +1 aesthetic, Echo Park gentrify flannel distillery freegan.",
		},
		{
			'title': 'Tote bag crucifix cornhole street art',
			'body': "Bespoke Truffaut freegan, YOLO Cosby sweater Marfa chillwave you probably haven't heard of them. Small batch mumblecore trust fund, street art Helvetica tousled before they sold out cornhole. Before they sold out synth next level Blue Bottle put a bird on it. Thundercats polaroid seitan, quinoa occupy direct trade PBR gastropub roof party. Cardigan Marfa you probably haven't heard of them gentrify umami pop-up. Gastropub retro wolf Echo Park post-ironic, farm-to-table twee Pinterest stumptown mustache food truck tofu quinoa photo booth jean shorts. Sustainable kogi scenester, keytar Tonx pop-up whatever chambray.",
		},
		{
			'title': 'Before they sold out chillwave',
			'body': "Pour-over mumblecore retro Williamsburg YOLO. Chambray farm-to-table Odd Future bicycle rights, wolf Tumblr Austin iPhone Etsy Brooklyn squid 3 wolf moon swag drinking vinegar. Authentic Portland Shoreditch, shabby chic letterpress butcher fingerstache. Chia flexitarian yr Bushwick kale chips Banksy mixtape VHS distillery, wayfarers meggings mlkshk Vice craft beer skateboard. Stumptown pour-over iPhone, biodiesel wayfarers Tumblr Schlitz. Disrupt gluten-free Thundercats semiotics mlkshk authentic, occupy sartorial sriracha polaroid. Seitan butcher shabby chic twee, banjo irony VHS Kickstarter ethical lo-fi direct trade brunch organic.",
		},
	];

	return app;
});